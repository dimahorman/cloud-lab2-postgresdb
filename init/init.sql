create type skill_lvl as enum ('HIGH', 'AVERAGE', 'LOW');
create type type_sensor as enum ('HUMIDITY', 'LIGHTING', 'NONE');
create type field_type as enum ('BANANA', 'CUCUMBER', 'POTATO', 'CORN', 'NONE');

create table farmers
(
    name varchar not null
        constraint farmers_pk
            primary key,
    age integer,
    birth_city varchar,
    skill_level skill_lvl
);


create unique index farmers_name_uindex
    on farmers (name);

create table farms
(
    id varchar not null
        constraint farms_pk
            primary key,
    owner varchar
        constraint farms_farmers_name_fk
            references farmers
            on update cascade on delete cascade
);


create unique index farms_id_uindex
    on farms (id);

create table farm_fields
(
    id integer not null
        constraint farm_fields_pk
            primary key,
    farm_id varchar
        constraint farm_fields_farms_id_fk
            references farms,
    type field_type
);

create unique index farm_fields_id_uindex
    on farm_fields (id);

create table sensors
(
    id integer not null
        constraint sensors_pk
            primary key,
    field_id integer
        constraint sensors_farm_fields_id_fk
            references farm_fields,
    gps_cords varchar,
    measured_value varchar,
    type type_sensor
);


create unique index sensors_id_uindex
    on sensors (id);


create table user_items
(
    id varchar not null
        constraint user_items_pk
            primary key,
    user_id varchar,
    coords varchar,
    height double precision,
    material varchar,
    weight double precision
);
